package elenas.web;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;


public class TestIgualarVotos extends FuncionesWeb
{

	@Before
	public void start() throws Exception {
		FuncionesWeb.setUp();
	}

	@Test
	public void igualarVotos() throws Exception {

		cantNotMuch = Integer.parseInt(cantidades(notMuch));
		cantTheSky = Integer.parseInt(cantidades(theSky));
		cantLaLaLa = Integer.parseInt(cantidades(laLaLa));

		Obj_Driver.findElement(By.xpath("//a[contains(text(),\"What's up?\")]")).click();
		int segundo;
		int tercero;

			int[] numeros = {cantNotMuch, cantTheSky, cantLaLaLa};
			int mayor = numeros[0];
			for (int x = 1; x < numeros.length; x++) {
				int numeroActual = numeros[x];
				if (numeroActual > mayor) {
					mayor = numeroActual;
				}
			}
			if (mayor==cantNotMuch){
				segundo = cantNotMuch - cantTheSky;
				tercero = cantNotMuch - cantLaLaLa;
				System.out.println("El mayer es Not Much y vamos a sumar "+ segundo +" votos a TheSky y "+tercero+" votos a LaLaLa");
				votarPor("choice2",segundo);
				votarPor("choice3",tercero);
			}else if(mayor==cantTheSky){
				segundo = cantTheSky - cantNotMuch;
				tercero = cantTheSky - cantLaLaLa;
				System.out.println("El mayor es The sky y vamos a sumar "+ segundo +" votos a Not Much y "+tercero+" votos a LaLaLa");
				votarPor("choice1",segundo);
				votarPor("choice3",tercero);
			}else{
				segundo = cantLaLaLa - cantNotMuch;
				tercero = cantLaLaLa - cantTheSky;
				System.out.println("El mayor es LaLaLa y vamos a sumar "+ segundo +" votos a Not Much y "+tercero+" votos a TheSky");
				votarPor("choice1",segundo);
				votarPor("choice2",tercero);

			}
			System.out.println("####################################");
		}

	@After
	public void finish() {
		System.out.println("Final del Test, se cierra la instancia del driver");
		Obj_Driver.quit();
		Obj_Driver = null;
	}
}


