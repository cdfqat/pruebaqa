package elenas.web;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;


public class TestVotaciones extends FuncionesWeb
{

	@Before
	public void start() throws Exception {
		FuncionesWeb.setUp();
	}

	@Test
	public void testVotar() {
		try
	    {
			Obj_Driver.findElement(By.xpath("//a[contains(text(),\"What's up?\")]")).click();
			String titulo = Obj_Driver.findElement(By.xpath("/html/body/h1")).getText();
			if (titulo.equals("What's up?")){
				System.out.println("Entramos en la Votaciones sobre "+titulo);
			}else{
				finish();
			}

			// Hacer las votaciones por las diferentes opciones
			//
			votarPor("choice1",5);
			votarPor("choice2",3);
			votarPor("choice3",2);
			totalVotos();
	    }
	    catch (Exception e)
	    {
	    	System.out.println(" ERROR en el Flujo probado, vafor validar");
	    	Obj_Driver.quit();
	    	Obj_Driver = null;
	    }		
	}
	@After
	public void finish() {
		System.out.println("Final del Test, se cierra la instancia del driver");
//		Obj_Driver.quit();
//		Obj_Driver = null;
	}



	}


