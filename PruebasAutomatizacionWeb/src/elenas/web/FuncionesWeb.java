package elenas.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FuncionesWeb {

    public static WebDriver Obj_Driver;
    public static String notMuch;
    public static String theSky;
    public static String laLaLa;
    public static String notMuchInit;
    public static String theSkyInit;
    public static String laLaLaInit;
    public static int cantNotMuch;
    public static int cantTheSky;
    public static int cantLaLaLa;
    public static String butVote ="//input[@type='submit' and contains(@value,'Vote')]";


    public static void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:\\CromeDriver\\chromedriver.exe");
        Obj_Driver = new ChromeDriver();
        Obj_Driver.get("http://127.0.0.1:8000/polls/1/results/");
        contInicialVotos();
        conteoVotos();
        Obj_Driver.get("http://127.0.0.1:8000/polls/");
        String urlWeb = Obj_Driver.getCurrentUrl();
        if (urlWeb.equals("http://127.0.0.1:8000/polls/")){
            System.out.println("Abre Navegador Chrome - exitoso.");
        }else {
            System.out.println("Abre Navegador Chrome - ERROR: url incorrecta:." + urlWeb);
        }
    }

    public void votarPor(String choise,int veces)  throws Exception {
        System.out.println("###############################################");
        System.out.println("Vamos a Votar " + veces + " veces por " + choise);
        for (int i = 1; i <= veces; i++) {
            System.out.println("Voto: " + i + " por " + choise);

            Obj_Driver.findElement(By.id(choise)).click();
            Obj_Driver.findElement(By.xpath("" + butVote + "")).click();
            conteoVotos();
            Obj_Driver.findElement(By.xpath("//a[@href='/polls/1/'][contains(.,'Vote again?')]")).click();
        }

    }
        public static void contInicialVotos() throws Exception {

            Thread.sleep(500);
            notMuchInit = Obj_Driver.findElement(By.xpath("//body/ul/li[1]")).getText();
            theSkyInit = Obj_Driver.findElement(By.xpath("//body/ul/li[2]")).getText();
            laLaLaInit = Obj_Driver.findElement(By.xpath("//body/ul/li[3]")).getText();

        }

        public static void conteoVotos() throws Exception {

            Thread.sleep(500);
            notMuch = Obj_Driver.findElement(By.xpath("//body/ul/li[1]")).getText();
            theSky = Obj_Driver.findElement(By.xpath("//body/ul/li[2]")).getText();
            laLaLa = Obj_Driver.findElement(By.xpath("//body/ul/li[3]")).getText();
        }
    public static void ganador() {



        System.out.println("####################################");
        int[] numeros = {cantNotMuch, cantTheSky, cantLaLaLa};
        int mayor = numeros[0];
        for (int x = 1; x < numeros.length; x++) {
            int numeroActual = numeros[x];
            if (numeroActual > mayor) {
                mayor = numeroActual;
            }
        }

        if (mayor==cantNotMuch){
            System.out.println("El ganador es Not much con "+ mayor +" votos");
        }else if(mayor==cantTheSky){
            System.out.println("El ganador es The sky con "+ mayor +" votos");
        }else{
            System.out.println("El ganador es La la la con "+ mayor +" votos");
        }
        System.out.println("####################################");
    }
    public static void perdedor() {

        System.out.println("###################################################");
                int[] otrosNumeros = {cantNotMuch, cantTheSky, cantLaLaLa};
                int menor = otrosNumeros[0];
                for (int x = 1; x < otrosNumeros.length; x++) {
                    int numeroActual = otrosNumeros[x];
                    if (numeroActual < menor) {
                        menor = numeroActual;
                    }
                }
        if (menor==cantNotMuch){
            System.out.println("La opción con menos votos es Not much con "+ menor +" votos");
        }else if(menor==cantTheSky){
            System.out.println("La opción con menos votos es The sky con "+ menor +" votos");
        }else{
            System.out.println("La opción con menos votos es La la la con "+ menor +" votos");
        }

        System.out.println("###################################################");
        }

        public static void totalVotos(){

            System.out.println("El conteo inicial de votos es:");
            System.out.println(notMuchInit);
            System.out.println(theSkyInit);
            System.out.println(laLaLaInit);
            System.out.println("##############################");
            System.out.println("Conteo total de Votos:");
            System.out.println(notMuch);
            System.out.println(theSky);
            System.out.println(laLaLa);
        }

    public static String cantidades(String choises){
        char [] cadena_div = choises.toCharArray();
        StringBuilder n = new StringBuilder();
        for (char c : cadena_div) {
            if (Character.isDigit(c)) {
                n.append(c);
            }
        }
        //System.out.println(n);
        return n.toString();
    }


}
