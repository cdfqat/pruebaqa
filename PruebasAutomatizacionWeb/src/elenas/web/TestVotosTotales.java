package elenas.web;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TestVotosTotales extends FuncionesWeb
{

	@Before
	public void start() throws Exception {
		FuncionesWeb.setUp();
	}

	@Test
	public void validarTotalVotos() {

		cantNotMuch = Integer.parseInt(cantidades(notMuch));
		cantTheSky = Integer.parseInt(cantidades(theSky));
		cantLaLaLa = Integer.parseInt(cantidades(laLaLa));

		System.out.println("La opción notMuch tiene: "+cantNotMuch+" Votos");
		System.out.println("La opción theSky tiene: "+cantTheSky+" Votos");
		System.out.println("La opción laLaLa tiene: "+cantLaLaLa+" Votos");
		System.out.println(" ");
		// Ganador
		FuncionesWeb.ganador();

		// con menos botos

		FuncionesWeb.perdedor();


	}
	@After
	public void finish() {
		System.out.println("Final del Test, se cierra la instancia del driver");
		Obj_Driver.quit();
		Obj_Driver = null;
	}
}


